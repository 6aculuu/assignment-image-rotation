//
// Created by Vasilii on 07.12.2023.
//

#include "transformations.h"

enum IMAGE_TRANSFORMATION_STATUS rotateImage(struct image *imageTransformed, const struct image *imageSource) {

    enum IMAGE_CREATE_STATUS imageCreateStatus = imageCreate(imageSource->height, imageSource->width, imageTransformed);

    if (imageCreateStatus != IMAGE_CREATE_SUCCESS) {
        return IMAGE_TRANSFORMATION_FAILED;
    }

    for (uint64_t x = 0; x < imageTransformed->width; x++) {
        for (uint64_t y = 0; y < imageTransformed->height; y++) {
            imageTransformed->data[y * imageTransformed->width + imageTransformed->width - (x + 1)] = imageSource->data[
                    x * imageSource->width + y];
        }
    }

    return IMAGE_TRANSFORMATION_SUCCESS;
}
