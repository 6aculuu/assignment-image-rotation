//
// Created by Vasilii on 07.12.2023.
//

#include "image.h"

char *ImageReadErrorMessages[] = {
        [IMAGE_READ_INVALID_SIGNATURE] = "Invalid signature of image",
        [IMAGE_READ_INVALID_HEADER] = "Invalid header of image",
        [IMAGE_READ_ALLOCATION_FAILED] = "Unable to allocate memory for image",
        [IMAGE_READ_UNSUPPORTED_BITNESS] = "Unsupported bitness of image, only 24 supported",
        [IMAGE_READ_BAD_DATA] = "Unable to read pixels of bmp image"
};

char *(ImageWriteErrorMessages)[] = {
        [IMAGE_WRITE_UNABLE_TO_WRITE_HEADER] = "Unable to write header of image",
        [IMAGE_WRITE_UNABLE_TO_WRITE_PIXELS] = "Unable to write pixels of image"
};

enum IMAGE_CREATE_STATUS imageCreate(const uint32_t width, const uint32_t height, struct image *image) {

    if (image==NULL){
        return IMAGE_CREATE_NULL_POINTER;
    }

    image->data = malloc(width * height * sizeof(struct pixel));

    if (image->data == NULL) {
        return IMAGE_CREATE_ALLOCATION_FAILED;
    }

    image->width = width;
    image->height = height;

    return IMAGE_CREATE_SUCCESS;
}

void freeImage(struct image image) {

    if (image.data) {
        free(image.data);
    }
}
