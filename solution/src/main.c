#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "image.h"
#include "transformations.h"
#include "utils.h"

int main(int argc, char **argv) {

    if (argc != 3) {
        perror("Incorrect number of arguments. Usage: ./image-transformer <source-image> <transformed-image>");
        return EXIT_FAILURE;
    }

    char *imageSourceFilename = argv[1];
    char *imageTransformedFilename = argv[2];

    FILE *imageSourceFile = fopen(imageSourceFilename, "rb");

    if (imageSourceFile == NULL) {
        perror("Unable to open source image");
        return OPEN_FAILED;
    }

    struct image imageSource = {0, 0};
    const enum IMAGE_READ_STATUS imageReadStatus = bmpRead(imageSourceFile, &imageSource);

    if (imageReadStatus != IMAGE_READ_SUCCESS) {
        freeImage(imageSource);
        perror(getStatusMessage(imageReadStatus, ImageReadErrorMessages));
        return READ_FAILED;
    }

    FILE *imageTransformedFile = fopen(imageTransformedFilename, "wb");

    if (imageTransformedFile == NULL) {
        perror("Unable to open transformed image");
        return OPEN_FAILED;
    }

    struct image imageTransformed = {0, 0};
    const enum IMAGE_TRANSFORMATION_STATUS imageTransformationStatus = rotateImage(&imageTransformed, &imageSource);

    if (imageTransformationStatus != IMAGE_TRANSFORMATION_SUCCESS) {
        freeImage(imageSource);
        freeImage(imageTransformed);
        perror("Image transformation failed");
        return TRANSFORMATION_FAILED;
    }

    const enum IMAGE_WRITE_STATUS imageWriteStatus = bmpWrite(imageTransformedFile, imageTransformed);

    if (imageWriteStatus != IMAGE_WRITE_SUCCESS) {
        freeImage(imageSource);
        freeImage(imageTransformed);
        perror(getStatusMessage(imageWriteStatus, ImageWriteErrorMessages));
        return WRITE_FAILED;
    }

    fclose(imageSourceFile);
    fclose(imageTransformedFile);

    freeImage(imageSource);
    freeImage(imageTransformed);

    return EXIT_SUCCESS;
}
