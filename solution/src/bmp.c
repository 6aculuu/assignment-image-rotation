//
// Created by Vasilii on 07.12.2023.
//

#include "bmp.h"

#define BMP_TYPE 0x4D42
#define BMP_BITNESS 24
#define BMP_SIZE 40
#define BMP_PLANES 1

#pragma pack(push, 1)
struct BmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum BMP_HEADER_READ_STATUS {
    BMP_HEADER_READ_SUCCESS = 0,
    BMP_HEADER_READ_FAILED
};

enum PIXEL_READ_STATUS {
    PIXEL_READ_SUCCESS = 0,
    PIXEL_READ_FAILED
};

enum PIXEL_WRITE_STATUS {
    PIXEL_WRITE_SUCCESS = 0,
    PIXEL_WRITE_FAILED
};

static enum BMP_HEADER_READ_STATUS bmpHeaderRead(FILE *imageFile, struct BmpHeader *bmpHeader) {

    uint8_t bmpHeaderReadResult = fread(bmpHeader, sizeof(struct BmpHeader), 1, imageFile);

    if (bmpHeaderReadResult != 0) {
        return BMP_HEADER_READ_SUCCESS;
    }

    return BMP_HEADER_READ_FAILED;
}

static uint8_t getPadding(const uint32_t width) {

    return (4 - (width * sizeof(struct pixel) % 4)) % 4;
}

static enum PIXEL_READ_STATUS bmpPixelsRead(FILE *imageFile, struct image *image) {

    uint8_t padding = getPadding(image->width);

    for (uint32_t y = 0; y < image->height; y++) {
        size_t pixelReadStatus = fread(&((image->data)[(image->width) * y]), sizeof(struct pixel), image->width,
                                       imageFile);
        size_t paddingStatus = fseek(imageFile, padding, SEEK_CUR);
        if (pixelReadStatus < image->width || paddingStatus != 0) {
            return PIXEL_READ_FAILED;
        }
    }

    return PIXEL_READ_SUCCESS;
}

static struct BmpHeader bmpHeaderCreate(const struct image *image) {

    uint32_t bmpHeaderSize = sizeof(struct BmpHeader);
    uint32_t width_length = image->width * sizeof(struct pixel);
    uint32_t padding = getPadding(width_length);
    uint32_t imageSize = image->height * (width_length + padding) + bmpHeaderSize;

    struct BmpHeader bmpHeader = {
            .bfType = BMP_TYPE,
            .bfileSize = imageSize,
            .bOffBits = bmpHeaderSize,
            .biSize = BMP_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_BITNESS};

    return bmpHeader;
}

static enum PIXEL_WRITE_STATUS bmpPixelsWrite(FILE *imageFile, struct image image) {

    uint16_t padding = getPadding(image.width);
    uint64_t some_value = 0;

    for (uint32_t y = 0; y < image.height; y++) {
        size_t pixelWriteStatus = (fwrite(&((image.data)[y * image.width]), sizeof(struct pixel), image.width,
                                          imageFile));
        size_t paddingStatus = fwrite(&some_value, 1, padding, imageFile);
        if (pixelWriteStatus != image.width || paddingStatus != padding) {
            return PIXEL_WRITE_FAILED;
        }
    }

    return PIXEL_WRITE_SUCCESS;
}

enum IMAGE_READ_STATUS bmpRead(FILE *imageFile, struct image *image) {

    struct BmpHeader bmpHeader;
    enum BMP_HEADER_READ_STATUS bmpHeaderReadStatus = bmpHeaderRead(imageFile, &bmpHeader);

    if (bmpHeaderReadStatus != BMP_HEADER_READ_SUCCESS) {
        return IMAGE_READ_INVALID_HEADER;
    }

    if (bmpHeader.bfType != BMP_TYPE) {
        return IMAGE_READ_INVALID_SIGNATURE;
    }

    if (bmpHeader.biBitCount != BMP_BITNESS) {
        return IMAGE_READ_UNSUPPORTED_BITNESS;
    }

    enum IMAGE_CREATE_STATUS imageCreateStatus = imageCreate(bmpHeader.biWidth, bmpHeader.biHeight, image);

    if (imageCreateStatus == IMAGE_CREATE_ALLOCATION_FAILED) {
        return IMAGE_READ_ALLOCATION_FAILED;
    }

    uint8_t offsetBitsResult = fseek(imageFile, bmpHeader.bOffBits, SEEK_SET);

    if (offsetBitsResult!=0){
        return IMAGE_READ_BAD_DATA;
    }

    enum PIXEL_READ_STATUS pixelReadStatus = bmpPixelsRead(imageFile, image);

    if (pixelReadStatus == PIXEL_READ_FAILED) {
        return IMAGE_READ_BAD_DATA;
    }

    return IMAGE_READ_SUCCESS;
}

enum IMAGE_WRITE_STATUS bmpWrite(FILE *imageFile, const struct image image) {

    struct BmpHeader bmpHeader;
    bmpHeader = bmpHeaderCreate(&image);

    if (fwrite(&bmpHeader, sizeof(struct BmpHeader), 1, imageFile) != 1) {
        return IMAGE_WRITE_UNABLE_TO_WRITE_HEADER;
    }

    enum PIXEL_WRITE_STATUS pixelWriteStatus = bmpPixelsWrite(imageFile, image);

    if (pixelWriteStatus == PIXEL_WRITE_FAILED) {
        return IMAGE_WRITE_UNABLE_TO_WRITE_PIXELS;
    }

    return IMAGE_WRITE_SUCCESS;
}
