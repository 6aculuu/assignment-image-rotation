//
// Created by Vasilii on 07.12.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_H

#include "image.h"

enum MAIN_ERRORS {
    OPEN_FAILED = 1,
    READ_FAILED,
    TRANSFORMATION_FAILED,
    WRITE_FAILED
};

char *getStatusMessage(int status, char *messages[]);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTILS_H
