//
// Created by Vasilii on 07.12.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

enum IMAGE_READ_STATUS {
    IMAGE_READ_SUCCESS = 0,
    IMAGE_READ_INVALID_SIGNATURE,
    IMAGE_READ_INVALID_HEADER,
    IMAGE_READ_ALLOCATION_FAILED,
    IMAGE_READ_UNSUPPORTED_BITNESS,
    IMAGE_READ_BAD_DATA,
};

extern char *ImageReadErrorMessages[];

enum IMAGE_WRITE_STATUS {
    IMAGE_WRITE_SUCCESS = 0,
    IMAGE_WRITE_UNABLE_TO_WRITE_HEADER,
    IMAGE_WRITE_UNABLE_TO_WRITE_PIXELS
};

extern char *ImageWriteErrorMessages[];

enum IMAGE_CREATE_STATUS {
    IMAGE_CREATE_SUCCESS = 0,
    IMAGE_CREATE_ALLOCATION_FAILED,
    IMAGE_CREATE_NULL_POINTER
};

enum IMAGE_CREATE_STATUS imageCreate(uint32_t width, uint32_t height, struct image *image);

void freeImage(struct image image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
