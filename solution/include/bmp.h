//
// Created by Vasilii on 07.12.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include <stdint.h>
#include <stdio.h>

#include "utils.h"

enum IMAGE_READ_STATUS bmpRead(FILE *imageFile, struct image *image);

enum IMAGE_WRITE_STATUS bmpWrite(FILE *imageFile, const struct image image);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
